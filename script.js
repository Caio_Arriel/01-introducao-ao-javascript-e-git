function createItems() {
    for (item = 0; item < 10; item++) {
        let text = "Hello World"
        switch (item) {
            //linha 1
            case 0:
                document.write(`<p> ${text} </p>`);
                break;
            // linha 2
            case 1:
                document.write(`<p>${text.toUpperCase()}</p>`);
                break;
            //linha 3
            case 2:
                document.write(`<p>${text.toLowerCase()}</p>`);
                break;
            //linha 4
            case 3:
                document.write(`<p>${text.split("").reverse().join("")}</p>`);
                break;
            //linha 5
            case 4:
                document.write(`<p>${text.split(" ").reverse().join("")}</p>`);
                break;
            //linha 6
            case 5:
                document.write(`<p> H <br> E <br> 
                L <br> L <br> O <br> <br> W <br> O <br> R <br> L <br> D 
                </p>`);

                break;
            //linha 7
            case 6:
                document.write(`<p class = "girar">${text}</p>`);
                break;
            //linha 8
            case 7:
                document.write(`<p class ="line8"> ${text.substring(0,6).split(" ")} <br> ${text.substring(6,12)} </p>`);
                break;
            //linha 9
            case 8:
                document.write(`<p class ="rotates"> ${text} </p>`);
                break;
            // linha 10
            case 9:
                document.write(`<p>${text.split("").reverse().join(" ")}</p>`);
                break;

            default:
                break;
        }
    }
}

createItems();
